/* maia.c: This is a simple test to Debian salsa
 * 
 * Developed by Gustavo Bacagine <gustavo.bacagine@protonmail.com>
 *
 * Date: 27/04/2021
 */

#include <stdio.h>
#include <stdlib.h>

typedef struct hello_t{
	void (*hello)(void);
} hello_t;

void hello(void);

int main(int argc, char **argv){
	hello_t *h = (hello_t *) malloc(sizeof(hello_t));
	h->hello = &hello;
	
	h->hello();
	printf("A simple test to Debian Salsa ;)\n");
	
	free(h);
	h = NULL;

	return 0;
}

void hello(void){
	puts("Hello World!!!");
}

